﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StringKata_05_08_2020
{
    public class Calculator
    {
        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            var defaultDelimiters = new[] { ',', '\n' };
            var numberList = numbers.StartsWith("//") ? GetNumbers(numbers) : numbers.Split(defaultDelimiters).ToList();

            return Sum(numberList);
        }

        private IEnumerable<string> GetNumbers(string numbers)
        {
            var delimiterAndNumbers = numbers.Split('\n');
            return delimiterAndNumbers[1].Split(delimiterAndNumbers[0].ToArray(), StringSplitOptions.RemoveEmptyEntries)
                .ToList();
        }

        private int Sum(IEnumerable<string> numberList)
        {
            var convertedNumbers = numberList.Select(int.Parse).ToList();

            ValidateNumbers(convertedNumbers);

            return convertedNumbers.Where(x => x < 1001).Sum();
        }

        private void ValidateNumbers(List<int> convertedNumbers)
        {
            if (convertedNumbers.Any(x => x < 0))
            {
                throw new Exception($"negatives not allowed {string.Join(" ", convertedNumbers.Where(x => x < 0))}");
            }
        }

    }
}

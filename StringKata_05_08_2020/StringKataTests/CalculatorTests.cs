﻿using System;
using NUnit.Framework;
using StringKata_05_08_2020;

namespace StringKataTests
{
    [TestFixture]
    public class CalculatorTests
    {
        [TestCase("", 0)]
        [TestCase("1", 1)]
        [TestCase("1,2", 3)]
        [TestCase("1,2,4,1", 8)]
        [TestCase("1\n2,3", 6)]
        [TestCase("//;\n1;2", 3)]
        public void Add_GivenNumbers_ShouldReturn_Sum(string numbers, int expectedResults)
        {
            //Arrange
            Calculator calculator = new Calculator();
            //Act
            var results = calculator.Add(numbers);
            //Assert
            Assert.AreEqual(expectedResults, results);
        }

       [TestCase("//;\n-1;2", "negatives not allowed -1")]
       [TestCase("1,2,-4, -1", "negatives not allowed -4 -1")]
        public void Add_GivenNegativeNumbers_ShouldThrowException(string numbers, string expectedResults)
        {
            //Arrange
            Calculator calculator = new Calculator();
            //Act
            var exception = Assert.Throws<Exception>(() => calculator.Add(numbers));
            //Assert
            Assert.AreEqual(expectedResults, exception.Message);
        }

        [TestCase("//;\n1;2000", 1)]
        [TestCase("1001,2", 2)]
        public void Add_GivenNumbersGreaterThousand_ShouldIngnoreThen(string numbers, int expectedResults)
        {
            //Arrange
            Calculator calculator = new Calculator();
            //Act
            var results = calculator.Add(numbers);
            //Assert
            Assert.AreEqual(expectedResults, results);
        }

        [TestCase("//[***]\n1***2***3", 6)]
        [TestCase("//[***]\n1***1", 2)]
        public void Add_GivenNumberWithAnyLengthDelimiters_ShouldReturnSum(string numbers, int expectedResults)
        {
            //Arrange
            Calculator calculator = new Calculator();
            //Act
            var results = calculator.Add(numbers);
            //Assert
            Assert.AreEqual(expectedResults, results);
        }

        [TestCase("//[*][%]\n1*2%3", 6)]
        [TestCase("//[***][%]\n1***2%3", 6)]
        public void Add_GivenNumberWithMultipleDelimiters_ShouldReturnSum(string numbers, int expectedResults)
        {
            //Arrange
            Calculator calculator = new Calculator();
            //Act
            var results = calculator.Add(numbers);
            //Assert
            Assert.AreEqual(expectedResults, results);
        }
    }
}

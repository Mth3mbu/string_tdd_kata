﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StringKata_04_08_2020
{
    public class Calculator
    {
        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
                return 0;

            var delimiters = GetDelimiters(numbers).ToArray();
            var numbersArray =
                numbers.Split(delimiters, StringSplitOptions.RemoveEmptyEntries).
                    Where(x => x != "//" && x != "//[" && x != "]" && int.Parse(x) <= 1000).ToArray();

            ValidateNegativeNumbers(numbersArray);

            return numbersArray.Sum(int.Parse);
        }

        private List<string> GetDelimiters(string numbers)
        {
            List<string> delimiters = new List<string> { ",", "\n" };

            if (!numbers.Contains("//") && !numbers.Contains("["))
                return delimiters;

            var customDelimiter = numbers.Substring(2);

            if (numbers.Contains("["))
            {
                delimiters.Add(customDelimiter.Substring(1, customDelimiter.IndexOf(']') - 1));
            }

            if (numbers.Contains("//") && !numbers.Contains("["))
            {
                delimiters.Add(customDelimiter.Substring(0, customDelimiter.IndexOf('\n')));
            }

            return delimiters;
        }

        public void ValidateNegativeNumbers(string[] numbersArray)
        {
            if (numbersArray.Any(x => int.Parse(x) < 0))
            {
                throw new Exception(
                    $"Negative numbers not allowed {string.Join(" ", numbersArray.Where(x => int.Parse(x) < 0))}");
            }
        }
    }
}

﻿using System;
using NUnit.Framework;
using StringKata_04_08_2020;

namespace StringKataTests
{
    [TestFixture]
    public class CalculatorTests
    {
        private Calculator _calculator;

        [SetUp]
        public void SetUp()
        {
            _calculator = new Calculator();
        }

        [Test]
        public void For_Add_Given_Empty_String_When_Adding_Then_Should_Return_Zero()
        {
            var results = _calculator.Add("");

            Assert.AreEqual(0,results);
        }

        [Test]
        public void For_Add_Given_One_Number_When_Adding_Then_Should_Return_Actual_Number()
        {
            var results = _calculator.Add("1");

            Assert.AreEqual(1, results);
        } 
        
        [Test]
        public void For_Add_Given_Two_Numbers_When_Adding_Then_Should_Return_Sum()
        {
            var results = _calculator.Add("1,2");

            Assert.AreEqual(3, results);
        }

        [Test]
        public void For_Add_Given_Unkown_Amount_Of_Numbers_When_Adding_Then_Should_Return_Sum()
        {
            var results = _calculator.Add("1,2,1,2");

            Assert.AreEqual(6, results);
        } 
        
        [Test]
        public void For_Add_Given_Numbers_Separated_By_NewLine_Delimeter_When_Adding_Then_Should_Return_Sum()
        {
            var results = _calculator.Add("1\n2,3");

            Assert.AreEqual(6, results);
        } 
        
        [Test]
        public void For_Add_Given_Numbers_Separated_By_Different_Delimeters_When_Adding_Then_Should_Return_Sum()
        {
            var results = _calculator.Add("//;\n1;2");

            Assert.AreEqual(3, results);
        }

        [Test]
        public void For_Add_Given_Negative_Numbers_When_Adding_Then_Should_Throw_Exception()
        {
            var exception = Assert.Throws<Exception>(() => _calculator.Add("//;\n1;-2"));

            Assert.AreEqual(exception.Message, "Negative numbers not allowed -2");
        } 
        
        [Test]
        public void For_Add_Given_Numbers_Greater_Than_Thousand_When_Adding_Then_Should_Ignore_Them()
        {
            var results = _calculator.Add("1,1001");

            Assert.AreEqual(1,results);
        }

        [Test]
        public void For_Add_Given_Numbers_With_Multiple_Delimiters_When_Adding_Then_Should_Return_Sum()
        {
            var results = _calculator.Add("//[***]\n1***2***3");

            Assert.AreEqual(6, results);
        }
    }
}

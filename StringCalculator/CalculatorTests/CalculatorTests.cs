﻿using System;
using NUnit.Framework;
using StringCalculator;

namespace CalculatorTests
{
    [TestFixture]
    public class CalculatorTests
    {
        [TestCase("", 0)]
        public void GivenEmptyString_ShouldReturnZero(string number, int expected)
        {
            // Arrange
            var calculator = new Calculator();
            // Act
            var results = calculator.Add(number);
            // Assert
            Assert.AreEqual(expected, results);
        }

        [TestCase("1", 1)]
        [TestCase("1,1", 2)]
        [TestCase("1\n1", 2)]
        [TestCase("1\n1,2", 4)]
        [TestCase("//;\n1;2", 3)]
        public void GivenNumbers_ShouldReturnSum(string number, int expected)
        {
            // Arrange
            var calculator = new Calculator();
            // Act
            var results = calculator.Add(number);
            // Assert
            Assert.AreEqual(expected, results);
        }

        [TestCase("//;\n1;-2", "negatives not allowed -2")]
        [TestCase("//;\n1;-2;4;-7", "negatives not allowed -2 -7")]
        public void GivenNegativeNumbers_ShouldThrowException(string number, string expected)
        {
            // Arrange
            var calculator = new Calculator();
            // Act
            var results = Assert.Throws<Exception>(() => calculator.Add(number));
            // Assert
            Assert.AreEqual(expected, results.Message);
        }

        [TestCase("//;\n1;2;1001", 3)]
        [TestCase("//;\n1;2000", 1)]
        public void GivenNumbersGreaterThanThousand_ShouldIgnoreThem(string number, int expected)
        {
            // Arrange
            var calculator = new Calculator();
            // Act
            var results = calculator.Add(number);
            // Assert
            Assert.AreEqual(expected, results);
        }

        [TestCase("//[***]\n1***2000", 1)]
        [TestCase("//[$$$$$]\n1$$$$$2", 3)]
        public void GivenMultipleDelimiters_ShouldReturnSum(string number, int expected)
        {
            // Arrange
            var calculator = new Calculator();
            // Act
            var results = calculator.Add(number);
            // Assert
            Assert.AreEqual(expected, results);
        }

        [TestCase("//[$][^]\n1$2^1", 4)]
        [TestCase("//[$][+]\n1$2+1", 4)]
        public void GivenDifferentDelimiters_ShouldReturnSum(string number, int expected)
        {
            // Arrange
            var calculator = new Calculator();
            // Act
            var results = calculator.Add(number);
            // Assert
            Assert.AreEqual(expected, results);
        }

        [TestCase("//[$$$$$][^^^^^]\n1$$$$$2^^^^^1", 4)]
        [TestCase("//[$$$$$][^^^^^][%%%%%]\n1$$$$$2^^^^^1%%%%%2", 6)]
        public void GivenMultipleDifferentDelimiters_ShouldReturnSum(string number, int expected)
        {
            // Arrange
            var calculator = new Calculator();
            // Act
            var results = calculator.Add(number);
            // Assert
            Assert.AreEqual(expected, results);
        }
    }
}

﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace StringCalculator
{
    public class Calculator
    {
        public object Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers)) return 0;

            var delimiter = new[] { ',', '\n' };
            var numbersList = numbers.StartsWith("//") ? GetNumbers(numbers) : numbers.Split(delimiter).ToList();

            return Sum(numbersList);
        }

        private int Sum(IEnumerable<string> numbers)
        {
            var convertedNumbers = numbers.Select(int.Parse).ToList();
            ValidateNumbers(convertedNumbers);
            return convertedNumbers.Where(x=>x<=1000).Sum(x => x);
        }
        private IEnumerable<string> GetNumbers(string numbers)
        {
            var delimitersAndNumbers = numbers.Split("\n");
            return delimitersAndNumbers[1].Split(delimitersAndNumbers[0].ToArray(),StringSplitOptions.RemoveEmptyEntries);
        }

        private void ValidateNumbers(List<int> numbers)
        {
            if (numbers.Any(x => x < 0))
            {
                throw new Exception($"negatives not allowed {string.Join(" ", numbers.Where(x => x < 0))}");
            }
        }
    }
}
